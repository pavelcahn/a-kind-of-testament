"use client"
import React from "react"
import styles from "./index.module.css"
import Image from "next/image"
import cover from "@/assets/image-with-text.png"

const Video = ({ allow, fallback }) => {
  const [state, setState] = React.useState(false)

  /**
   * Display vimeo player
   */
  const handleClick = React.useCallback(
    (evt) => {
      evt.preventDefault()
      setState(true)
    },
    [setState]
  )
  return (
    <div className={styles.container}>
      {state ? (
        allow ? (
          <div className={styles.video}>
            <iframe
              src="https://player.vimeo.com/video/901515394"
              frameBorder="0"
              allow="autoplay; fullscreen; picture-in-picture"
            ></iframe>
          </div>
        ) : (
          <p>{fallback}</p>
        )
      ) : (
        <div className={styles.image}>
          <Image
            src={cover}
            sizes="(max-width: 800px) 100vw, 800px"
            alt="A Kind of Testament"
          />
          <button className={styles.play} onClick={handleClick}></button>
        </div>
      )}
    </div>
  )
}

export default Video
