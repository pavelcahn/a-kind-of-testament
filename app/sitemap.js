const domain = "https://www.akindoftestament.com"

export default async function sitemap() {
  return [
    {
      url: domain + "/",
      lastModified: new Date("2024-1-10"),
    },
  ]
}
