import styles from "./page.module.css"
import { headers } from "next/headers"
import Video from "@/components/video"

const forbidden = [
  "fr",
  "be",
  "ch",
  "mc",
  "ad",
  "mu",
  "mg",
  "km",
  "lu",
  "bj",
  "bf",
  "bi",
  "cm",
  "km",
  "cd",
  "cg",
  "ci",
  "dj",
  "ga",
  "gn",
  "gw",
  "gf",
  "ml",
  "mr",
  "ne",
  "cf",
  "rw",
  "sn",
  "tg",
  "td",
  "dz",
  "ao",
  "bw",
  "cv",
  "eg",
  "er",
  "et",
  "gm",
  "gh",
  "ht",
  "ke",
  "lr",
  "ly",
  "mw",
  "ma",
  "mz",
  "na",
  "ng",
  "ug",
  "st",
  "sc",
  "sl",
  "so",
  "ss",
  "sd",
  "sz",
  "tz",
  "tn",
  "zm",
  "sw",
  "nc",
  "pf",
  "mq",
  "gp",
  "nc",
  "bl",
  "mf",
  "pm",
  "wf",
  "yt",
  "re",
]

export default function Home() {
  const headersList = headers()
  const countryCode = headersList.get("x-vercel-ip-country") || ""
  const allow = forbidden.indexOf(countryCode.toLowerCase()) === -1

  return (
    <main className={styles.main}>
      <Video
        allow={allow}
        fallback={`Visitors from ${countryCode} are not allowed to see this media`}
      />
    </main>
  )
}
