import styles from "./not-found.module.css"

export const metadata = {
  title: "404 - Not found",
}

export default async function Page(props) {
  return (
    <main className={styles.main}>
      <h1>404</h1>
    </main>
  )
}
